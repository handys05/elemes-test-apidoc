/**
 * @api {get} /course List Course
 * @apiName Get Courses
 * @apiDescription List Untuk Melihat Seluruh Course
 * @apiGroup Users
 * @apiVersion 0.0.0
 *
 * @apiSuccessExample Response 200 OK
 {
    "response_code": 200,
    "response_message": "success",
    "response_timestamp": "2021-11-12T08:27:32.596+00.00",
    "error": null,
    "page": 1,
    "totalPage": 1,
    "totalData": 8,
    "limit": 10,
    "keyword": "",
    "order": "desc",
    "column": "updatedAt",
    "data": [
        {
            "id": 1,
            "author": "Hallo",
            "name": "Node JS For Dummy",
            "description": "Node js",
            "image_url": "http://res.cloudinary.com/ariefhnd/image/upload/v1636702511/tnd0sm0797awgh6j7duw.jpg",
            "price": 500000,
            "isDelete": 0,
            "createdBy": null,
            "updatedBy": 1,
            "createdAt": "2021-11-11T11:55:29.000Z",
            "updatedAt": "2021-11-12T07:35:12.000Z"
        },
        {
            "id": 8,
            "author": "Arief Handiansyah",
            "name": "Mastering PHP",
            "description": "Mastering PHP",
            "image_url": "http://res.cloudinary.com/ariefhnd/image/upload/v1636702501/n1lpy9d5aus32wutc6ob.jpg",
            "price": 250000,
            "isDelete": 0,
            "createdBy": 1,
            "updatedBy": 1,
            "createdAt": "2021-11-12T07:30:30.000Z",
            "updatedAt": "2021-11-12T07:35:01.000Z"
        },
        {
            "id": 7,
            "author": "Arief Handiansyah",
            "name": "Mastering PHP",
            "description": "Mastering PHP",
            "image_url": "http://res.cloudinary.com/ariefhnd/image/upload/v1636702496/promp32w86humpxawnrm.jpg",
            "price": 250000,
            "isDelete": 0,
            "createdBy": 1,
            "updatedBy": 1,
            "createdAt": "2021-11-12T07:27:10.000Z",
            "updatedAt": "2021-11-12T07:34:56.000Z"
        },
        {
            "id": 6,
            "author": "Arief Handiansyah",
            "name": "Mastering PHP",
            "description": "Mastering PHP",
            "image_url": "http://res.cloudinary.com/ariefhnd/image/upload/v1636702489/w1xz4ppiuflngqrmkcrw.jpg",
            "price": 250000,
            "isDelete": 0,
            "createdBy": 1,
            "updatedBy": 1,
            "createdAt": "2021-11-12T07:25:26.000Z",
            "updatedAt": "2021-11-12T07:34:50.000Z"
        }
    ]
  }
 *@apiErrorExample Error 500
 {
    "response_code": 500,
    "response_message": error_message,
    "response_timestamp": "2021-11-12T08:28:44.263+00.00",
    "error": null
 }
 *
 */

 /**
 * @api {post} /course/add Add Course
 * @apiUse AuthHeader
 * @apiName Add Course
 * @apiDescription Untuk menambahkan course (hanya admin)
 * @apiGroup Admin
 * @apiVersion 0.0.0
 * 
 * @apiParam (Body Params) {string} author (required)
 * @apiParam (Body Params) {String} name (required)
 * @apiParam (Body Params) {integer} price (required)
 * @apiParam (Body Params) {String} description (optional)
 * @apiParam (Body Params) {String} image (optional) diisi dengan image yang telah diconvert ke base46
 *
 *@apiSuccessExample Example Request Body
  {
    "author": "Arief Handiansyah",
    "name": "Mastering PHP",
    "price": 250000,
    "description": "Mastering PHP",
    "image": "base64 image"
 }
 * @apiSuccessExample Response 200 OK
 {
    "response_code": 200,
    "response_message": "success",
    "response_data": {
        "id": 9,
        "author": "Arief Handiansyah",
        "name": "Mastering PHP",
        "description": "Mastering PHP",
        "price": "250000",
        "image_url": "http://res.cloudinary.com/ariefhnd/image/upload/v1636706401/jrlqvci9cqrcphxrupxo.jpg",
        "createdBy": 1,
        "updatedAt": "2021-11-12T08:40:02.169Z",
        "createdAt": "2021-11-12T08:40:02.169Z"
    },
    "response_timestamp": "2021-11-12T08:40:02.290+00.00",
    "error": null
 }
 *@apiErrorExample Error 500
 {
    "response_code": 500,
    "response_message": error_message,
    "response_timestamp": "2021-11-12T08:28:44.263+00.00",
    "error": null
 }
 *
 */

 /**
 * @api {get} /course/:id Get Course
 * @apiName Detail Course
 * @apiDescription Untuk melihat detail course
 * @apiGroup Users
 * @apiVersion 0.0.0
 * 
 * @apiSuccessExample Response 200 OK
 {
    "response_code": 200,
    "response_message": "success",
    "response_data": {
        "id": 1,
        "author": "Hallo",
        "name": "Node JS For Dummy",
        "description": "Node js",
        "image_url": "http://res.cloudinary.com/ariefhnd/image/upload/v1636702511/tnd0sm0797awgh6j7duw.jpg",
        "price": 500000,
        "isDelete": 0,
        "createdBy": null,
        "updatedBy": 1,
        "createdAt": "2021-11-11T11:55:29.000Z",
        "updatedAt": "2021-11-12T07:35:12.000Z"
    },
    "response_timestamp": "2021-11-12T08:46:42.353+00.00",
    "error": null
 }
 *@apiErrorExample Error 500
 {
    "response_code": 500,
    "response_message": error_message,
    "response_timestamp": "2021-11-12T08:28:44.263+00.00",
    "error": null
 }
 *
 */

 /**
 * @api {put} /course/:id Update Course
 * @apiUse AuthHeader
 * @apiName Update Course
 * @apiDescription Untuk mengupdate course (hanya admin)
 * @apiGroup Admin
 * @apiVersion 0.0.0
 * 
 * @apiParam (Body Params) {string} author (optional)
 * @apiParam (Body Params) {String} name (optional)
 * @apiParam (Body Params) {integer} price (optional)
 * @apiParam (Body Params) {String} description (optional)
 * @apiParam (Body Params) {String} image (optional) diisi dengan image yang telah diconvert ke base46
 *
 *@apiSuccessExample Example Request Body
  {
    "author": "Arief Handiansyah",
    "name": "Mastering PHP",
    "price": 250000,
    "description": "Mastering PHP",
    "image": "base64 image"
 }
 * @apiSuccessExample Response 200 OK
 {
    "response_code": 200,
    "response_message": "success",
    "response_data": {
        "id": 9,
        "author": "Arief Handiansyah",
        "name": "Mastering PHP",
        "description": "Mastering PHP",
        "price": "250000",
        "image_url": "http://res.cloudinary.com/ariefhnd/image/upload/v1636706401/jrlqvci9cqrcphxrupxo.jpg",
        "createdBy": 1,
        "updatedAt": "2021-11-12T08:40:02.169Z",
        "createdAt": "2021-11-12T08:40:02.169Z"
    },
    "response_timestamp": "2021-11-12T08:40:02.290+00.00",
    "error": null
 }
 *@apiErrorExample Error 500
 {
    "response_code": 500,
    "response_message": error_message,
    "response_timestamp": "2021-11-12T08:28:44.263+00.00",
    "error": null
 }
 *
 */

 /**
 * @api {post} /course/delete/:id Delete Course
 * @apiName Delete Course
 * @apiUse AuthHeader
 * @apiDescription Untuk menghapus course (hanya admin)
 * @apiGroup Admin
 * @apiVersion 0.0.0
 * 
 * @apiSuccessExample Response 200 OK
 {
    "response_code": 200,
    "response_message": "success",
    "response_timestamp": "2021-11-12T08:50:12.809+00.00",
    "error": null
 }
 *@apiErrorExample Error 500
 {
    "response_code": 500,
    "response_message": error_message,
    "response_timestamp": "2021-11-12T08:28:44.263+00.00",
    "error": null
 }
 *
 */

 /**
 * @api {post} /course/enroll/:id Enroll Course
 * @apiUse AuthHeader
 * @apiName Enroll Course
 * @apiDescription untuk mengikuti course
 * @apiGroup Users
 * @apiVersion 0.0.0
 * 
 * @apiSuccessExample Response 200 OK
 {
    "response_code": 200,
    "response_message": "success",
    "response_data": {
        "id": 17,
        "user_id": 6,
        "course_id": "8",
        "createdBy": 6,
        "updatedAt": "2021-11-12T08:51:31.606Z",
        "createdAt": "2021-11-12T08:51:31.606Z"
    },
    "response_timestamp": "2021-11-12T08:51:31.646+00.00",
    "error": null
 }
 *@apiErrorExample Error 500
 {
    "response_code": 500,
    "response_message": error_message,
    "response_timestamp": "2021-11-12T08:28:44.263+00.00",
    "error": null
 }
 *
 */

 /**
 * @api {get} /course/free-course List Free Course
 * @apiName Get Free Courses
 * @apiDescription List Untuk Melihat Seluruh Free Course
 * @apiGroup Users
 * @apiVersion 0.0.0
 *
 * @apiSuccessExample Response 200 OK
 {
    "response_code": 200,
    "response_message": "success",
    "response_timestamp": "2021-11-12T09:54:35.723+00.00",
    "error": null,
    "page": 1,
    "totalPage": 1,
    "totalData": 3,
    "limit": 10,
    "keyword": "",
    "order": "desc",
    "column": "updatedAt",
    "data": [
        {
            "id": 4,
            "author": "Arief Handiansyah",
            "name": "PHP For Dummy",
            "description": "Free Course",
            "image_url": "http://res.cloudinary.com/ariefhnd/image/upload/v1636702433/vaypnvdz5gbfkenni0lu.jpg",
            "price": 0,
            "isDelete": 0,
            "createdBy": null,
            "updatedBy": 1,
            "createdAt": "2021-11-11T11:56:38.000Z",
            "updatedAt": "2021-11-12T07:33:54.000Z"
        },
        {
            "id": 3,
            "author": "Arief Handiansyah",
            "name": "PHP For Dummy",
            "description": "PHP",
            "image_url": "http://res.cloudinary.com/ariefhnd/image/upload/v1636702429/yv1hsxzvbdcm8l9iq605.jpg",
            "price": 0,
            "isDelete": 0,
            "createdBy": null,
            "updatedBy": 1,
            "createdAt": "2021-11-11T11:56:24.000Z",
            "updatedAt": "2021-11-12T07:33:49.000Z"
        },
        {
            "id": 2,
            "author": "Arief Handiansyah",
            "name": "PHP For Dummy",
            "description": "PHP",
            "image_url": "http://res.cloudinary.com/ariefhnd/image/upload/v1636702425/oabuqjyq7ygy72aa1jo5.jpg",
            "price": 0,
            "isDelete": 0,
            "createdBy": null,
            "updatedBy": 1,
            "createdAt": "2021-11-11T11:56:00.000Z",
            "updatedAt": "2021-11-12T07:33:45.000Z"
        }
    ]
 }
 *@apiErrorExample Error 500
 {
    "response_code": 500,
    "response_message": error_message,
    "response_timestamp": "2021-11-12T08:28:44.263+00.00",
    "error": null
 }
 *
 */

 /**
 * @api {get} /course/popular-course List Popular Course
 * @apiName Get Popular Courses
 * @apiDescription List Untuk Melihat Seluruh Popular Course
 * @apiGroup Users
 * @apiVersion 0.0.0
 *
 * @apiSuccessExample Response 200 OK
 {
    "response_code": 200,
    "response_message": "success",
    "response_timestamp": "2021-11-12T09:57:04.017+00.00",
    "error": null,
    "page": 1,
    "totalPage": 1,
    "totalData": 2,
    "limit": 10,
    "keyword": "",
    "order": "desc",
    "column": "updatedAt",
    "data": [
        {
            "author": "Hallo",
            "name": "Node JS For Dummy",
            "total_enrollment": 10
        },
        {
            "author": "Arief Handiansyah",
            "name": "PHP For Dummy",
            "total_enrollment": 6
        }
    ]
 }
 *@apiErrorExample Error 500
 {
    "response_code": 500,
    "response_message": error_message,
    "response_timestamp": "2021-11-12T08:28:44.263+00.00",
    "error": null
 }
 *
 */

 /**
 * @api {get} /course?sortBy=:sortbycategory Sort Course
 * @apiName Sort Course
 * @apiDescription List Untuk Menyortir Seluruh Course dengan parameter sortBy
 * @apiGroup Users
 * @apiVersion 0.0.0
 * @apiParam (Query Params) {string} sortBy bisa diisi dengan (highPrice, lowPrice, free)
 *
 * @apiSuccessExample Response 200 OK
 {
    "response_code": 200,
    "response_message": "success",
    "response_timestamp": "2021-11-12T09:58:55.314+00.00",
    "error": null,
    "page": 1,
    "totalPage": 1,
    "totalData": 9,
    "limit": 10,
    "keyword": "",
    "order": "desc",
    "column": "updatedAt",
    "data": [
        {
            "id": 1,
            "author": "Hallo",
            "name": "Node JS For Dummy",
            "description": "Node js",
            "image_url": "http://res.cloudinary.com/ariefhnd/image/upload/v1636702511/tnd0sm0797awgh6j7duw.jpg",
            "price": 500000,
            "isDelete": 0,
            "createdBy": null,
            "updatedBy": 1,
            "createdAt": "2021-11-11T11:55:29.000Z",
            "updatedAt": "2021-11-12T07:35:12.000Z"
        },
        {
            "id": 5,
            "author": "Arief Handiansyah",
            "name": "Mastering PHP",
            "description": "Mastering PHP",
            "image_url": "http://res.cloudinary.com/ariefhnd/image/upload/v1636702438/mn4bgrhp4fjrfr7f07wc.jpg",
            "price": 250000,
            "isDelete": 0,
            "createdBy": 1,
            "updatedBy": 1,
            "createdAt": "2021-11-12T00:49:11.000Z",
            "updatedAt": "2021-11-12T07:33:58.000Z"
        },
        {
            "id": 6,
            "author": "Arief Handiansyah",
            "name": "Mastering PHP",
            "description": "Mastering PHP",
            "image_url": "http://res.cloudinary.com/ariefhnd/image/upload/v1636702489/w1xz4ppiuflngqrmkcrw.jpg",
            "price": 250000,
            "isDelete": 0,
            "createdBy": 1,
            "updatedBy": 1,
            "createdAt": "2021-11-12T07:25:26.000Z",
            "updatedAt": "2021-11-12T07:34:50.000Z"
        }
    ]
}
 *@apiErrorExample Error 500
 {
    "response_code": 500,
    "response_message": error_message,
    "response_timestamp": "2021-11-12T08:28:44.263+00.00",
    "error": null
 }
 *
 */

 /**
 * @api {get} /course?keyword=:keyword Search Course By Author Or Name
 * @apiName Search Course
 * @apiDescription List Untuk Mencari Seluruh Course dengan parameter keyword
 * @apiGroup Users
 * @apiVersion 0.0.0
 * @apiParam (Query Params) {string} sortBy bisa diisi dengan Author atau Name dari course itu sendiri
 *
 * @apiSuccessExample Response 200 OK
 {
    "response_code": 200,
    "response_message": "success",
    "response_timestamp": "2021-11-12T09:58:55.314+00.00",
    "error": null,
    "page": 1,
    "totalPage": 1,
    "totalData": 9,
    "limit": 10,
    "keyword": "",
    "order": "desc",
    "column": "updatedAt",
    "data": [
        {
            "id": 1,
            "author": "Hallo",
            "name": "Node JS For Dummy",
            "description": "Node js",
            "image_url": "http://res.cloudinary.com/ariefhnd/image/upload/v1636702511/tnd0sm0797awgh6j7duw.jpg",
            "price": 500000,
            "isDelete": 0,
            "createdBy": null,
            "updatedBy": 1,
            "createdAt": "2021-11-11T11:55:29.000Z",
            "updatedAt": "2021-11-12T07:35:12.000Z"
        },
        {
            "id": 5,
            "author": "Arief Handiansyah",
            "name": "Mastering PHP",
            "description": "Mastering PHP",
            "image_url": "http://res.cloudinary.com/ariefhnd/image/upload/v1636702438/mn4bgrhp4fjrfr7f07wc.jpg",
            "price": 250000,
            "isDelete": 0,
            "createdBy": 1,
            "updatedBy": 1,
            "createdAt": "2021-11-12T00:49:11.000Z",
            "updatedAt": "2021-11-12T07:33:58.000Z"
        },
        {
            "id": 6,
            "author": "Arief Handiansyah",
            "name": "Mastering PHP",
            "description": "Mastering PHP",
            "image_url": "http://res.cloudinary.com/ariefhnd/image/upload/v1636702489/w1xz4ppiuflngqrmkcrw.jpg",
            "price": 250000,
            "isDelete": 0,
            "createdBy": 1,
            "updatedBy": 1,
            "createdAt": "2021-11-12T07:25:26.000Z",
            "updatedAt": "2021-11-12T07:34:50.000Z"
        }
    ]
}
 *@apiErrorExample Error 500
 {
    "response_code": 500,
    "response_message": error_message,
    "response_timestamp": "2021-11-12T08:28:44.263+00.00",
    "error": null
 }
 *
 */