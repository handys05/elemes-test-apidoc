/**
 * @api {get} /admin/users List User
 * @apiName Get Users
 * @apiDescription List Untuk Melihat Seluruh User (Hanya Admin)
 * @apiGroup Admin
 * @apiVersion 0.0.0
 *
 * @apiSuccessExample Response 200 OK
 {
    "response_code": 200,
    "response_message": "success",
    "response_timestamp": "2021-11-12T09:32:04.975+00.00",
    "error": null,
    "page": 1,
    "totalPage": 1,
    "totalData": 10,
    "limit": 10,
    "keyword": "",
    "order": "desc",
    "column": "updatedAt",
    "data": [
        {
            "id": 12,
            "name": "testing",
            "email": "testing@gmail.com",
            "password": "0c49442a416d8ae0afe5db9f959af3b26ded47dab26d216d4734bd2898efd2b8",
            "role": "member",
            "isDelete": 0,
            "createdBy": null,
            "updatedBy": null,
            "createdAt": "2021-11-12T09:25:23.000Z",
            "updatedAt": "2021-11-12T09:25:23.000Z"
        },
        {
            "id": 2,
            "name": "Member 1",
            "email": "member1@gmail.com",
            "password": "0c49442a416d8ae0afe5db9f959af3b26ded47dab26d216d4734bd2898efd2b8",
            "role": "member",
            "isDelete": 0,
            "createdBy": null,
            "updatedBy": null,
            "createdAt": "2021-11-12T00:18:07.000Z",
            "updatedAt": "2021-11-12T00:18:07.000Z"
        },
        {
            "id": 3,
            "name": "Member 2",
            "email": "member2@gmail.com",
            "password": "0c49442a416d8ae0afe5db9f959af3b26ded47dab26d216d4734bd2898efd2b8",
            "role": "member",
            "isDelete": 0,
            "createdBy": null,
            "updatedBy": null,
            "createdAt": "2021-11-12T00:18:07.000Z",
            "updatedAt": "2021-11-12T00:18:07.000Z"
        },
        {
            "id": 4,
            "name": "Member 3",
            "email": "member3@gmail.com",
            "password": "0c49442a416d8ae0afe5db9f959af3b26ded47dab26d216d4734bd2898efd2b8",
            "role": "member",
            "isDelete": 0,
            "createdBy": null,
            "updatedBy": null,
            "createdAt": "2021-11-12T00:18:07.000Z",
            "updatedAt": "2021-11-12T00:18:07.000Z"
        },
        {
            "id": 5,
            "name": "Member 4",
            "email": "member4@gmail.com",
            "password": "0c49442a416d8ae0afe5db9f959af3b26ded47dab26d216d4734bd2898efd2b8",
            "role": "member",
            "isDelete": 0,
            "createdBy": null,
            "updatedBy": null,
            "createdAt": "2021-11-12T00:18:07.000Z",
            "updatedAt": "2021-11-12T00:18:07.000Z"
        },
        {
            "id": 6,
            "name": "Member 5",
            "email": "member5@gmail.com",
            "password": "0c49442a416d8ae0afe5db9f959af3b26ded47dab26d216d4734bd2898efd2b8",
            "role": "member",
            "isDelete": 0,
            "createdBy": null,
            "updatedBy": null,
            "createdAt": "2021-11-12T00:18:07.000Z",
            "updatedAt": "2021-11-12T00:18:07.000Z"
        },
        {
            "id": 7,
            "name": "Member 6",
            "email": "member6@gmail.com",
            "password": "0c49442a416d8ae0afe5db9f959af3b26ded47dab26d216d4734bd2898efd2b8",
            "role": "member",
            "isDelete": 0,
            "createdBy": null,
            "updatedBy": null,
            "createdAt": "2021-11-12T00:18:07.000Z",
            "updatedAt": "2021-11-12T00:18:07.000Z"
        },
        {
            "id": 8,
            "name": "Member 7",
            "email": "member7@gmail.com",
            "password": "0c49442a416d8ae0afe5db9f959af3b26ded47dab26d216d4734bd2898efd2b8",
            "role": "member",
            "isDelete": 0,
            "createdBy": null,
            "updatedBy": null,
            "createdAt": "2021-11-12T00:18:07.000Z",
            "updatedAt": "2021-11-12T00:18:07.000Z"
        },
        {
            "id": 9,
            "name": "Member 8",
            "email": "member8@gmail.com",
            "password": "0c49442a416d8ae0afe5db9f959af3b26ded47dab26d216d4734bd2898efd2b8",
            "role": "member",
            "isDelete": 0,
            "createdBy": null,
            "updatedBy": null,
            "createdAt": "2021-11-12T00:18:07.000Z",
            "updatedAt": "2021-11-12T00:18:07.000Z"
        },
        {
            "id": 11,
            "name": "Member 10",
            "email": "member10@gmail.com",
            "password": "0c49442a416d8ae0afe5db9f959af3b26ded47dab26d216d4734bd2898efd2b8",
            "role": "member",
            "isDelete": 0,
            "createdBy": null,
            "updatedBy": null,
            "createdAt": "2021-11-12T00:18:07.000Z",
            "updatedAt": "2021-11-12T00:18:07.000Z"
        }
    ]
 }
 *@apiErrorExample Error 500
 {
    "response_code": 500,
    "response_message": error_message,
    "response_timestamp": "2021-11-12T08:28:44.263+00.00",
    "error": null
 }
 *
 */

/**
 * @api {post} /course/:id Delete User
 * @apiName Delete User
 * @apiUse AuthHeader
 * @apiDescription Untuk menghapus user (hanya admin)
 * @apiGroup Admin
 * @apiVersion 0.0.0
 * 
 * @apiSuccessExample Response 200 OK
 {
    "response_code": 200,
    "response_message": "success",
    "response_timestamp": "2021-11-12T08:50:12.809+00.00",
    "error": null
 }
 *@apiErrorExample Error 500
 {
    "response_code": 500,
    "response_message": error_message,
    "response_timestamp": "2021-11-12T08:28:44.263+00.00",
    "error": null
 }
 *
*/

/**
 * @api {post} /user/register User Register
 * @apiUse AuthHeader
 * @apiName Register User
 * @apiDescription Untuk registrasi user
 * @apiGroup Users
 * @apiVersion 0.0.0
 * 
 * @apiParam (Body Params) {string} name (required)
 * @apiParam (Body Params) {String} email (required)
 * @apiParam (Body Params) {integer} password (required)
 *
 *@apiSuccessExample Example Request Body
  {
    "name": "testing",
    "email": "testing@gmail.com",
    "password": "123456789"
 }
 * @apiSuccessExample Response 200 OK
 {
    "response_code": 200,
    "response_message": "success registering user",
    "response_data": {
        "id": 12,
        "name": "testing",
        "email": "testing@gmail.com",
        "password": "0c49442a416d8ae0afe5db9f959af3b26ded47dab26d216d4734bd2898efd2b8",
        "role": "member",
        "updatedAt": "2021-11-12T09:25:23.042Z",
        "createdAt": "2021-11-12T09:25:23.042Z"
    },
    "response_timestamp": "2021-11-12T09:25:23.080+00.00",
    "error": null
}
 *@apiErrorExample Error 500
 {
    "response_code": 500,
    "response_message": error_message,
    "response_timestamp": "2021-11-12T08:28:44.263+00.00",
    "error": null
 }
 *
 */