/**
 * @api {GET} - Getting Started
 * @apiGroup Home
 * @apiVersion 0.0.0
 * @apiParam (Global Params) [URI=http://localhost:4000]
*/

/**
 * @api {POST} /auth/login Login
 * @apiGroup Auth
 * @apiVersion 0.0.0
 * @apiParam (Body Params) {String} email (required)
 * @apiParam (Body Params) {String} password (required)
 *
 * @apiSuccessExample Example Request Body
  {
    "email": "admin@gmail.com",
    "password": "123456789"
  }
 * @apiSuccessExample Response 200 OK
  {
    "response_code": 200,
    "response_message": "success",
    "response_data": {
        "email": "admin@gmail.com",
        "token": "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VyX2lkIjoxLCJpYXQiOjE2MzY3MTE5NTEsImV4cCI6MTYzNjc5ODM1MX0.4mbYqvMDrMg1qAXiSSgk_eCI5VSRiwOGgOzCspDFR3E"
    },
    "response_timestamp": "2021-11-12T10:12:31.196+00.00",
    "error": null
}
 * @apiErrorExample Error 500
 *{
    "response_code": 500,
    "response_message": error_message,
    "response_data": null,
    "response_timestamp": "2021-11-12T10:12:44.230+00.00",
    "error": null
 }
 *
 *
*/