/**
 * @api {get} /admin/course-statistic Simple Statistic Course
 * @apiName Admin
 * @apiUse AuthHeader
 * @apiDescription Untuk melihat Simple Statistic Course (hanya admin)
 * @apiGroup Admin
 * @apiVersion 0.0.0
 * 
 * @apiSuccessExample Response 200 OK
 {
    "response_code": 200,
    "response_message": "success",
    "response_timestamp": "2021-11-12T08:50:12.809+00.00",
    "error": null
 }
 *@apiErrorExample Error 500
 {
    "response_code": 500,
    "response_message": error_message,
    "response_timestamp": "2021-11-12T08:28:44.263+00.00",
    "error": null
 }
 *
*/