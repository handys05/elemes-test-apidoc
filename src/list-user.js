/**
 * @api User Member dan Admin
 * @apiGroup Home
 * @apiVersion 0.0.0
 * @apiDescription List User Member dan Admin
 * 
 *  @apiSuccessExample List User Yang Bisa Dipakai
 * {
        name: "Admin",
        email: "admin@gmail.com",
        password: "123456789",
        role: "admin"
    },
    {
        name: "Member 1",
        email: "member1@gmail.com",
        password: "123456789",
        role: "member"
    },
    {
        name: "Member 2",
        email: "member2@gmail.com",
        password: "123456789",
        role: "member"
    },
    {
        name: "Member 3",
        email: "member3@gmail.com",
        password: "123456789",
        role: "member"
    },
    {
        name: "Member 4",
        email: "member4@gmail.com",
        password: "123456789",
        role: "member"
    },
    {
        name: "Member 5",
        email: "member5@gmail.com",
        password: "123456789",
        role: "member"
    },
    {
        name: "Member 6",
        email: "member6@gmail.com",
        password: "123456789",
        role: "member"
    },
    {
        name: "Member 7",
        email: "member7@gmail.com",
        password: "123456789",
        role: "member"
    },
    {
        name: "Member 8",
        email: "member8@gmail.com",
        password: "123456789",
        role: "member"
    },
    {
        name: "Member 9",
        email: "member9@gmail.com",
        password: "123456789",
        role: "member"
    },
    {
        name: "Member 10",
        email: "member10@gmail.com",
        password: "123456789",
        role: "member"
    }
*/